define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name gnio2ClientApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the gnio2ClientApp
   */
  angular.module('gnio2ClientApp.controllers.MainCtrl', [])
          .controller('MainCtrl', function ($scope, $location, Region, City, UserCollected) {

            ///Obtengo una instancia de Usuario, y le agrego unos parametros iniciales
            var fnIntitializeUserCollected = function fnIntitializeUserCollectedF() {
              var userCollected = new UserCollected;
              userCollected.country = 49;
              userCollected.region = 601;
              userCollected.email = '';///MACHETE: si no lo pongo, el reset no lo borra bien :/
              
              return userCollected;
            }
            
            $scope.userCollected = fnIntitializeUserCollected();

            $scope.regions = Region.query({country: $scope.userCollected.country}); ///Traigo las regiones
            $scope.cities = []; ///ciudades

            ///traifo las ciudades segun la region
            $scope.getCities = function (region) {
              $scope.cities = City.query({region: region}, function(){
                
                ///Si existen ciudades, escojo la primera
                if($scope.cities.length)
                  $scope.userCollected.city = $scope.cities[0].id;
                else
                  $scope.userCollected.city = null;
              });
              
            }

            ///Funcion para guardar 
            $scope.save = function (userform) {
              if (userform.$valid) {
                $scope.userCollected.$save().then(function (data) {
                  $location.path("/thanks");
                }, function (data) {
                  console.log(data.data);
                });
              }
            }

            ///Funcion para borrar el formulario
            $scope.clear = function (userform) {
              $scope.userCollected = fnIntitializeUserCollected();
              if (userform) {
                userform.$setPristine();
                userform.$setUntouched();
              }
              
              $scope.getCities($scope.userCollected.region);
            }

            ///Dice si un campo es valido o no
            $scope.validateField = function (field) {
              return field.$invalid && field.$touched;
            }

            $scope.getCities($scope.userCollected.region);
            
          });
});
