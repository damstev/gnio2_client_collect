define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc function
   * @name gnio2ClientApp.controller:AboutCtrl
   * @description
   * # AboutCtrl
   * Controller of the gnio2ClientApp
   */
  angular.module('gnio2ClientApp.controllers.ThanksCtrl', [])
    .controller('ThanksCtrl', function ($scope) {
      
    });
});
