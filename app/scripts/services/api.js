define(['angular'], function (angular) {
  'use strict';

  /**
   * @ngdoc service
   * @name gnio2ClientApp.Api
   * @description
   * # Api
   * Factory in the gnio2ClientApp.
   */
  var domainUrl = "http://localhost:8000/";
  angular.module('gnio2ClientApp.services', [])
          .factory('ApiConfig', function () {
            var config = {
              domainUrl: "http://localhost:8000/",
            }

            return config;
          })
          
          .factory('Region', function ($resource, ApiConfig) {
            var url = ApiConfig.domainUrl + "location/api/regions/:id";

            return $resource(url, {id: '@id', },
                    {
                      update: {
                        method: 'PUT',
                        params: {id: '@id'}
                      },
                    });
          })

          .factory('City', function ($resource, ApiConfig) {
            var url = ApiConfig.domainUrl + "location/api/cities/:id";

            return $resource(url, {id: '@id', },
                    {
                      update: {
                        method: 'PUT',
                        params: {id: '@id'}
                      },
                    });
          })

          .factory('UserCollected', function ($resource, ApiConfig) {
            var url = ApiConfig.domainUrl + "collecting/api/usercollected/:id";

            return $resource(url, {id: '@id', },
                    {
                      update: {
                        method: 'PUT',
                        params: {id: '@id'}
                      },
                    });
          });
});
