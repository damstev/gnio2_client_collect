/*jshint unused: vars */
define(['angular', 'controllers/main', 'controllers/thanks', 'services/api']/*deps*/, function (angular, MainCtrl, AboutCtrl)/*invoke*/ {
  'use strict';

  /**
   * @ngdoc overview
   * @name gnio2ClientApp
   * @description
   * # gnio2ClientApp
   *
   * Main module of the application.
   */
  return angular
          .module('gnio2ClientApp', ['gnio2ClientApp.controllers.MainCtrl',
            'gnio2ClientApp.controllers.ThanksCtrl',
            'gnio2ClientApp.services',
            /*angJSDeps*/
            'ngCookies',
            'ngResource',
            'ngSanitize',
            'ngRoute',
            'ngAnimate',
            'ngTouch'
          ])
          .config(function ($routeProvider, $httpProvider, $resourceProvider) {

            // Don't strip trailing slashes from calculated URLs
            $resourceProvider.defaults.stripTrailingSlashes = false;

            $routeProvider
                    .when('/', {
                      templateUrl: 'views/main.html',
                      controller: 'MainCtrl'
                    })
                    .when('/thanks', {
                      templateUrl: 'views/thanks.html',
                      controller: 'ThanksCtrl'
                    })
                    .otherwise({
                      redirectTo: '/'
                    });
          });
});
